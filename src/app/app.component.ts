import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'starWarSearcher';
  categories: string[] ;
  allowed_choices: number[];

  ngOnInit() {
    this.categories = ["people", "planets", "films", "vehicles", "species", "starships"];
    this.allowed_choices=[1,2,3,4,5];
  }

  makeSelection(selectorForm){
    let chosen_category = selectorForm.form.value.category;
    let chosen_number = selectorForm.form.value.number;

    let objects = this.getObjectsfromAPI(chosen_category);
        
  }

  constructor(private httpClient:HttpClient) { }

  getObjectsfromAPI(category:string)   {

   let obj = this.httpClient.get('https://swapi.co/api/'+category+'/');
      obj.subscribe( (response)=>{
      console.log(response);
   });

  }

}












// getObjectsfromAPI(category:string)  {
//   let results = [];
//   this.httpClient.get('https://swapi.co/api/'+category+'/')
//   .subscribe( (response)=>{
//     console.log(response);

//   });

// }