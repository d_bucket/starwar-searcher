import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent implements OnInit {

  categories: string[] ;
  allowed_choices: number[];

  constructor() {
    
   }
  
   getData(selectorForm){
    alert( JSON.stringify(selectorForm.form.value, null, 2));
  }

  ngOnInit() {
    this.categories = ["people", "planets", "vehicles", "species", "starships"];
    this.allowed_choices=[1,2,3,4,5];
  }

}
